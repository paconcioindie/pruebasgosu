package pruebas


public class Propiedades {

    var _bar : String as Bar = 'Bar'
    var _solo_lectura : String as readonly SoloLectura = 'Solo lectura'
    var _asd = 'Asd'

    property get Asd() : String {
        return _asd
    }

    property set Asd(value : String) {
        if(value == 'Qwe') {
            throw 'Asd no puede ser Qwe'
        }
        _asd = value;
    }

    static function main(args : String[]) {
        var propiedades = new Propiedades();

        print('_bar = ${propiedades._bar}')
        print('Bar = ${propiedades.Bar}')
        print('_solo_lectura = ${propiedades._solo_lectura}')
        print('SoloLectura = ${propiedades.SoloLectura}')

        propiedades.Bar = 'Bar 2'
        print('Bar = ${propiedades.Bar}')
        propiedades._bar = 'Bar 3'
        print('_bar = ${propiedades._bar}')
        print('Bar = ${propiedades.Bar}')

        propiedades._solo_lectura = 'Solo lectura 2'
        print('_solo_lectura = ${propiedades._solo_lectura}')

        print('Asd = ${propiedades.Asd}')
    }

}