package pruebas

uses java.lang.Runnable


class Interfaces implements Runnable {

    delegate _runnable represents Runnable

//    property get Impl : Runnable {
//        return _runnable
//    }

    property set Impl(r : Runnable) {
        _runnable = r
    }

    static function main(args : String[]) {
        var inter = new Interfaces()
        inter.Impl = new Runnable() {
            function run() {
                print('Hola, delegando...')
            }
        }
        inter.run()
    }

}