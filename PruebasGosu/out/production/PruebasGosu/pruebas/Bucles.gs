package pruebas


class Bucles {

    static function main(args : String[]) {
        var list = {'one', 'two', 'three', 5, true}
        print('list = ${list}')

        print('* muestro los valores de la lista')
        for(num in list) {
            print(num)
        }

        print('* muestro los indices de la lista')
        for(num in list index i) {
            print('${i} : ${num}')
        }

        print('* uso "iterator" para borrar los elementos de la lista')
        for(num in list iterator iter) {
            iter.remove();
        }
        print('list = ${list}')

        print('* del 0 al 5')
        for(i in 0..5) {
            print(i)
        }

        print('* 5 valores desde el 0')
        for(i in 0..|5) {
            print(i)
        }

        print('* mayor al 0 y menor al 5')
        for(i in 0|..|5) {
            print(i)
        }
    }

}
