package pruebas

uses java.util.List


class Clases {
    var _names : List<String>

    construct(names : List<String>) {
        _names = names
    }

    function printNames(prefix : String = '> ') {
        for(n in _names) {
            print(prefix + n)
        }
    }

    property get Names() : List<String> {
        return _names
    }

    static function main(args : String[]) {
        var clases = new Clases({'Juan', 'Roberto', 'Pablo'})
        clases.printNames('Nombre: ')
        print(clases.Names)
        clases.printNames()
        clases.printNames(:prefix = '- ')
    }

}
